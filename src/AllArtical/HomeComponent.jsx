import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { api } from "../api/api";
import CardComponent from "./CardComponent";


export default function HomeComponent() {
  const [data, setData] = useState([]);
  
  const deleteArticle = (id) => {
    const newData = data.filter((data) => data._id !== id);
    setData(newData);
    api.delete(`/articles/${id}`).then((r) => {
      console.log(r.data.message);
    });
  };

  useEffect(() => {
    api.get("/articles").then((res) => {
      setData(res.data.payload);
      console.log(res);
    });
  }, []);

 

  return (
    <div>
      <Container>
        <div className="d-flex p-3 justify-content-between">
          <h1>All Articles</h1>
          <Button
            as={NavLink}
            to="/AddArticle"
            className="btn-light text-center align-self-center fw-bold"
          >
            New Article
          </Button>
        </div>
        <Row>
          {data.map((product, index) => (
            <Col xs={6} sm={3} md={2} key={index}>
              <CardComponent product={product} deleteArticle={deleteArticle} />
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
}
