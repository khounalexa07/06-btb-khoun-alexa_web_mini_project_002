import React from "react";
import { Button, Card, Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function CardCategory( {product,deleteCategory}) {
  const navigate = useNavigate();
  const handleUpdate = () => {
    navigate("/UpdateCategory", { state: { ...product } });
  };

  const HandleView = () => {
    navigate("/ViewCategory", { state: { ...product } });
  };

  return (
    <>
      <Card className="my-2 " style={{ borderRadius: "20px" }}>

        <Card.Body>
          <Card.Title style={{ height: "50px" }} className="title text" >
            {product.name}
          </Card.Title>
          <div className="d-flex flex-column mb-2">
            <Button className="mb-2" onClick={HandleView} variant="warning">
              View
            </Button>
            <Button
              className="mb-2"
              variant="danger"
              onClick={() => deleteCategory(product._id)}
            >
              Delete
            </Button>

            <Button variant="primary" onClick={() => handleUpdate(product._id)}>
              Edit
            </Button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
}
