import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function CardComponent({ product, deleteArticle  }) {
  const navigate = useNavigate();
  const handleUpdate = () => {
    navigate("/UpdateArticle", { state: { ...product } });
  };

  const HandleView = () => {
    navigate("/ViewArtical", { state: { ...product } });
  };


  return (
    <>
      <Card className="my-2 " style={{ borderRadius: "20px" }}>
        <Card.Img
          variant="top"
          style={{ height: "200px", objectFit: "cover", borderRadius: "20px" }}
          className="mb-2 p-2"
          src={
            product.image ??
            "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"
          }
        />
        <Card.Body>
          <Card.Title style={{ height: "50px" }} className="title text" >
            {product.title}
          </Card.Title>
          <Card.Text className="text " style={{ height: "100px" }}>
            {product.description ?? "No info"}
          </Card.Text>
          <div className="d-flex flex-column mb-2">
            <Button className="mb-2" onClick={HandleView} variant="warning">
              View
            </Button>
            <Button
              className="mb-2"
              variant="danger"
              onClick={() => deleteArticle(product._id)}
            >
              Delete
            </Button>

            <Button variant="primary" onClick={() => handleUpdate(product._id)}>
              Edit
            </Button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
}
