import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap';
import { useLocation } from 'react-router-dom';
import Swal from 'sweetalert2';
import { api } from '../api/api';

export default function UpdateCategory() {



  const [id, setId] = useState();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [isPublished, setIsPublished] = useState(true);
  const [category, setCategory] = useState("");


  const handleSubmit = () => {
    api
      .put("/category/" + id, { name  })
      .then((res) => console.log(res.data.message))
      .then(Swal.fire("Good job!", "You clicked the button!", "success"));
  };

  const handleTitleChange = (e) => {
    setName(e.target.value);
  };

  useEffect(() => {
    console.log(name);
  }, [name]);





  const location = useLocation();
  const oldData = location.state
  useEffect(()=>{
    setId(oldData._id)
    setName(oldData.name)
   
  }, [location])



  return (
    <div>UpdateCategory


<Container className="w-50">
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            value={name}
            placeholder="title"
            onChange={handleTitleChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicCheckbox">
        
        </Form.Group>
        <Button variant="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </Form>
    </Container>












    </div>
  )
}
