
import React, { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";

import { api } from "../api/api";

export default function AddArtical() {

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isPublished, setIsPublished] = useState(true);
  const [image, setImage] = useState("");
  const [imageUrl, setImageUrl] = useState();



  const handleSubmit = () => {
    api.post("/articles", { title, description, isPublished, image})
      .then((res) => console.log(res.data.message))
      .then(Swal.fire("Good job!", "You clicked the button!", "success"));
  };

  const handleTitleChange = (e) => {
    setTitle(e.target.value);
  };

  useEffect(() => {
    console.log(title);
  }, [title]);

  const handleDesChange = (e) => {
    setDescription(e.target.value);
  };

  const isPublishedChange = (e) => {
    setIsPublished(e.target.checked);
    console.log(e.target.checked);
  };

  const handleImageChange = (e) => {
    console.log("e.target.files[0] " + e.target.files[0]);
    console.log(URL.createObjectURL(e.target.files[0]));
    setImageUrl(URL.createObjectURL(e.target.files[0]));
    const formData = new FormData();
    formData.append("image", e.target.files[0]);
    console.log("FormData : ", formData.get("image"));
    api
      .post("/images", formData)
      .then((res) => setImage(res?.data?.payload?.url));
  };

  return (
    <div>

<Container className="w-50">
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Title</Form.Label>
          <Form.Control
            type="text"
            placeholder="title"
            onChange={handleTitleChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            value={description}
            placeholder="description"
            onChange={handleDesChange}
          />
        </Form.Group>
        <Form.Group controlId="formFile" className="mb-3">
          <Form.Label>Image File</Form.Label>
          <Form.Control type="file" onChange={handleImageChange} />
        </Form.Group>
        <img src={imageUrl?? "https://imgs.search.brave.com/mPnAWHZtek4bIGonwiL-rqHCDcJn7p_JhSGmZ7HQ6Lo/rs:fit:223:225:1/g:ce/aHR0cHM6Ly90c2U0/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5H/NHJiSEZCczNHdktW/VlhpbzcydFJBQUFB/QSZwaWQ9QXBp"} alt="preview" style={{ height: "200px" }} />
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check
            type="checkbox"
            checked={isPublished}
            label="Is Published"
            onChange={isPublishedChange}
          />
        </Form.Group>
        <Button variant="primary" onClick={handleSubmit}>
          Submit
        </Button>
      </Form>
    </Container>



    </div>
  )
}
