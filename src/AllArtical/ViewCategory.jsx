import React from "react";
import { Button, Container, Row } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";

export default function ViewCategory() {
  const location = useLocation();
  const oldData = location.state;
  const navigate = useNavigate();

  return (
    <div>
      <h1>ViewCategory</h1>

      <Container className="my-2">
        <Row className="view-card p-3">
          <Button
            onClick={() => navigate("/Categories")}
            style={{ width: "10%" }}
            className="m-3 btn-light fw-bold"
          >
            Back
          </Button>
          <div className="" style={{ display: "flex", margin: 5 }}>
            <div style={{ flex: 1, height: "100%" }}>
              <h4 className="p-3">{oldData.name}</h4>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  );
}
