import React, { useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { api } from "../api/api";
import CardCategory from "./CardCategory";
import { NavLink} from "react-router-dom";

export default function Categories() {


const [data, setData] = useState([]);
  
const deleteCategory = (id) => {
  const newData = data.filter((data) => data._id !== id);
  setData(newData);
  api.delete(`/category/${id}`).then((r) => {
    console.log(r.data.message);
  });
};

useEffect(() => {
  api.get("/category").then((res) => {
    setData(res.data.payload);
    console.log(res);
  });
}, []);


  return (
    <Container>
    <div className="d-flex p-3 justify-content-between">
      <h1>All Category</h1>
      <Button
        as={NavLink}
        to="/AddCategory"
        className="btn-light text-center align-self-center fw-bold"
      >
        New Category
      </Button>
    </div>
    <Row>
      {data.map((product, index) => (
        <Col xs={6} sm={3} md={2} key={index}>
          <CardCategory product={product} deleteCategory={deleteCategory} />
        </Col>
      ))}
    </Row>
  </Container>
  );
}
