import React, { useEffect, useState } from "react";
import { Button, Container, Form } from "react-bootstrap";
import Swal from "sweetalert2";
import { api } from "../api/api";

export default function AddCategory() {
  const [name, setName] = useState("");

  const handleSubmit = () => {
    api
      .post("/category", { name })
      .then((res) => console.log(res.data.message))
      .then(Swal.fire("Good job!", "You clicked the button!", "success"));
  };

  const handleTitleChange = (e) => {
    setName(e.target.value);
  };

  useEffect(() => {
    console.log(name);
  }, [name]);

  return (
    <div>
      <h1>AddCategory</h1>

      <Container className="w-50">
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Category Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Name"
              onChange={handleTitleChange}
            />
          </Form.Group>
          <Button variant="primary" onClick={handleSubmit}>
            Submit
          </Button>
        </Form>
      </Container>
    </div>
  );
}
