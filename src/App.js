import {  Route, Routes } from 'react-router-dom';
import './App.css';
import NavigationBar from './component/NavigationBar';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Card, Container, Row } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { api } from './api/api';
import HomeComponent from './AllArtical/HomeComponent';
import ViewArtical from './AllArtical/ViewArtical';
import UpdateArticle from './AllArtical/UpdateArticle';
import AddArtical from './AllArtical/AddArtical';
import Categories from'./AllArtical/Categories';
import UpdateCategory from './AllArtical/UpdateCategory';
import AddCategory from './AllArtical/AddCategory';
import ViewCategory from './AllArtical/ViewCategory';
function App() {


  const [ article ,setArtical ]= useState([])
  const [ category ,setGetagory ]= useState([])


  useEffect(()=> {
      api.get('/articles').then((res) => setArtical(res.data.payload))

      api.get('/category').then((r)=> setGetagory(r.data.payload))
    },[])




    const handleDelete = (id) => {
      const newData = article.filter((data) => data._id !== id);
      setArtical(newData);
      api.delete(`/articles/${id}`).then((r) => {
        console.log(r.data.message);
      });
    };



  return (
    <div className="App">
      
      
<NavigationBar/>
 <Container>

 <Row>







</Row>

</Container>  
 <Container>
<Routes>

        <Route path='/' element={<HomeComponent/>}/> 
        <Route path='/ViewArtical' element={<ViewArtical/>}/> 
        <Route path='/UpdateArticle' element={<UpdateArticle/>}/> 
        <Route path='/AddArticle' element={<AddArtical/>}/> 
        <Route path='/Categories' element={<Categories/>}/> 
        <Route path='/UpdateCategory' element={<UpdateCategory/>}/> 
        <Route path='/AddCategory' element={<AddCategory/>}/> 
        <Route path='/ViewCategory' element={<ViewCategory/>}/> 

       


</Routes>
</Container>



    </div>


  );
}

export default App;
