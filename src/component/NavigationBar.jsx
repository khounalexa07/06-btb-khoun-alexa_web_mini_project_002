import React from "react";
import { Container, Nav, Navbar, Row } from "react-bootstrap";
import { NavLink } from "react-router-dom";
export default function NavigationBar() {
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Container>



          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link as={NavLink} to="/" href="#AllArticle ">
                Home
              </Nav.Link>
              <Nav.Link  as={NavLink} to="/Categories" href="#Category ">Categories</Nav.Link>
            </Nav>
          </Navbar.Collapse>
 

        </Container>
      </Navbar>
    </div>
  );
}
